#!/usr/bin/python

from   pymongo       import MongoClient
from   bson.objectid import ObjectId
from   collections   import Counter, defaultdict
from   wityu         import WityuDB
from   knn           import Knn

import heapq  # extract top k
import pickle # serialize log data
import math
import sys
import codecs
import locale

"""
    entry point
    - connect to db and prepare data
    - build bigram model
    - recommend top k next stations to every stations
"""
def main():

    wityu        = WityuDB('localhost', 27017)
    model_bigram = Bigram(True)
    model_knn    = Knn()
    k            = 5
    userStation  = None

    # save time for preprocessing userlogs
    try:
        raise IOError() # force rereading database
        with open('user_stations','r')  as f: userStation = pickle.load(f)
    except IOError as e:
        print "Preprocessing playStation data..."
        users = wityu.get_user_set_by_actions(['playStation'])['playStation']
        userStation = {}
        for user in users: userStation[user] = wityu.get_playStations_by_user(user)
        with open('user_stations','wb') as f: pickle.dump(userStation,f)

    # build bigram and knn model
    print "Building models..."
    for user in userStation: model_bigram.add_station_sequence( userStation[user] )
    model_knn.add_user_station_dict(userStation)

    # write suggestions back into db
    print "Generating predictions..."
    """
    for sid_prev in wityu.get_stations():
        sp   = wityu.get_station_by_id(sid_prev)
        c_sp = model_bigram.count_1[sid_prev]

        # skip stations no one has ever listened to
        if c_sp == 0 or 'deleted' in sp and sp['deleted'] or 'hidden' in sp and sp['hidden']: continue
        if 'title' not in sp or sid_prev not in model_bigram.count_2: continue

        print sp['title']
        doc = [t[1] for t in model_bigram.recommend_next_stations(sid_prev,k)]
        wityu.update_station(sid_prev, {'suggest_bigram': doc});

        doc = [t[1] for t in model_knn.recommend_next_stations(sid_prev,k,Knn.SIM_COSINE)]
        wityu.update_station(sid_prev, {'suggest_knn_cosine' : doc});

        doc = [t[1] for t in model_knn.recommend_next_stations(sid_prev,k,Knn.SIM_JACCARD)]
        wityu.update_station(sid_prev, {'suggest_knn_jaccard' : doc});
    """
    show_suggestions(model_bigram,model_knn,wityu)

"""
    print results to stdout
"""
def show_suggestions(model_bigram,model_knn,wityu,k=5):
    # suggest max top k next stations for each
    n_th     = 0
    stations = sorted(wityu.get_stations(),key=model_bigram.count_1.get)
    for sid_prev in stations:
        sp   = wityu.get_station_by_id(sid_prev)
        c_sp = model_bigram.count_1[sid_prev]

        # skip stations no one has ever listened to
        if c_sp == 0 or 'deleted' in sp and sp['deleted'] or 'hidden' in sp and sp['hidden']: continue

        n_th += 1
        print "%3d Current: %s  (visited %d times)" % (n_th, sp['title'], c_sp)

        print "  - bigram"
        if sid_prev not in model_bigram.count_2:
            print "    Next (no suggestions)"
            print
            continue
        for t in model_bigram.recommend_next_stations(sid_prev,k):
            (cnt,sid_next) = t
            sn = wityu.get_station_by_id(sid_next)
            print "    Next %5.2f%% (%2d)  %s" % (100*model_bigram.get_p(sid_prev,sid_next),model_bigram.count_2[sid_prev][sid_next],sn['title'])
        print

        print "  - knn cosine similarity"
        for t in model_knn.recommend_next_stations(sid_prev,k,Knn.SIM_COSINE):
            (sim,sid_next) = t
            sn = wityu.get_station_by_id(sid_next)
            print "    Next  (%2.2f)      %s" % (sim,sn['title'])
        print

        print "  - knn jaccard coefficient"
        for t in model_knn.recommend_next_stations(sid_prev,k,Knn.SIM_JACCARD):
            (sim,sid_next) = t
            sn = wityu.get_station_by_id(sid_next)
            print "    Next  (%2.2f)      %s" % (sim,sn['title'])
        print


"""
    build probabilistic model
    for a given current station A
    recommend station B which maximizes P(B|A)
"""
class Bigram:

    def __init__(self,invert=False):
        self.count_1 = Counter()
        self.count_2 = defaultdict(lambda:Counter())
        self.invert  = invert

    def add_station_sequence(self,stations):
        if len(stations)<2: return
        # a  index to previous station
        # b  index to next station
        for a in range(len(stations)-1):
            b = a+1
            self.count_1[stations[a]] += 1
            self.count_2[stations[b]][stations[a]] += 1
            if self.invert:
                self.count_2[stations[a]][stations[b]] += 1
        self.count_1[stations[-1]] += 1

        if self.invert:
            for a in stations:
                self.count_1[a] += 1

    def recommend_next_stations(self,sid_prev,k=5):
        if sid_prev not in self.count_2: return []

        # suggest highest P(B|A), break ties with P(B)
        N = float( sum(self.count_1.values()) )
        candidates = [(self.count_2[sid_prev][sid]+self.count_1[sid]/N,sid) for sid in self.count_2[sid_prev]]
        return heapq.nlargest(k, candidates)

    def get_p(self, sid_prev, sid_next):
        return self.count_2[sid_prev][sid_next] / float(sum(self.count_2[sid_prev].values()))
    

"""
class Knn:
    SIM_COSINE  = 0
    SIM_JACCARD = 1
    def __init__(self):
        # key = station, value = {users who listened to this station}
        self.stations   = defaultdict(lambda:Counter()) 
        self.dotcache   = {}
        self.setcache   = {}
        self.sim_fn_map = {0:self.cosine_similarity,1:self.jaccard_coefficient}

    def add_user_station_dict(self, user_stations):
        for user in user_stations:
            for station in user_stations[user]:
                self.stations[station][user] += 1
    
    def recommend_next_stations(self,sid_prev,k=5,sim_fn=0):
        candidates = []
        for sid_next in self.stations:
            if sid_prev == sid_next: continue
            fn = self.sim_fn_map[sim_fn]
            candidates.append( (fn(sid_prev,sid_next),sid_next) )
        return heapq.nlargest(k, candidates)

    def jaccard_coefficient(self,sid1,sid2):
        s1,s2 = self.get_set(sid1), self.get_set(sid2)
        if len(s1)==0 or len(s2)==0: return 0.0
        return float(len(s1&s2))/len(s1|s2)

    def get_set(self,sid):
        if sid in self.setcache: return self.setcache[sid]
        self.setcache[sid] = set( self.stations[sid].keys() )
        return self.setcache[sid]
        
    def cosine_similarity(self,sid1,sid2):
        len1 = math.sqrt( self.dot_product(sid1,sid1) )
        len2 = math.sqrt( self.dot_product(sid2,sid2) )
        if len1 == 0 or len2 == 0: return 0.0
        v1v2 = self.dot_product(sid1,sid2)
        return float(v1v2)/(len1*len2)
         
    def dot_product(self,sid1,sid2): 
        if   (sid1,sid2) in self.dotcache: return self.dotcache[(sid1,sid2)]
        elif (sid2,sid1) in self.dotcache: return self.dotcache[(sid2,sid1)]
        v1,v2 = self.stations[sid1],self.stations[sid2]
        val   = sum([v1[sid]*v2[sid] for sid in v1])
        self.dotcache[(sid1,sid2)] = val
        return val


class WityuDB:

    def __init__(self, host='localhost',port=27017):
        # connect to wityu db
        self.db = MongoClient(host, port).wityu_music_db

        # obtain collections
        self.users    = self.db.users
        self.songs    = self.db.songs
        self.userlogs = self.db.userlogs
        self.stations = self.db.stations
        self.docs     = {'users':self.users,'songs':self.songs,'userlogs':self.userlogs,'stations':self.stations}

    def get_stations(self):                  return [c['_id'] for c in self.stations.find({},{'_id':1})]
    def get_doc_by_id(self, doc, _id):       return self.docs[doc].find_one({'_id':_id})
    def get_station_by_id(self, station_id): return self.get_doc_by_id('stations',station_id)
    def get_song_by_id(self, station_id):    return self.get_doc_by_id('songs',station_id)
    def get_station_title_by_id(self, station_id): return self.get_station_by_id(station_id)['title']

    def get_user_set_by_actions(self, actions=[]):
        users = dict()
        for cursor in self.userlogs.find({'user':{'$exists':True},'action':{'$in':actions}},{'user':1,'action':1}):
            curr_action = cursor['action']
            curr_user   = cursor['user']
            if curr_action not in users: users[curr_action] = set()
            users[curr_action].add(curr_user)

        for cursor in self.userlogs.find({'session':{'$exists':True},'action':{'$in':actions}},{'session':1,'action':1}):
            curr_action = cursor['action']
            curr_user   = cursor['session']
            if curr_action not in users: users[curr_action] = set()
            users[curr_action].add(curr_user)
        return users

    def get_playStations_by_user(self, user):
        output = []
        cursors = self.userlogs.find({'user':user,'action':'playStation'},{'station':1,'_id':1})
        prev=''
        for c in cursors:
            s = self.get_station_by_id(c['station'])
            if prev == s['_id']: continue
            prev = s['_id']
            output.append(c['station'])
        return output

    def update_station(self, sid, suggestion_doc):
        self.stations.update({'_id':sid}, {'$set':suggestion_doc});
"""

if __name__ == '__main__':
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
    main()
