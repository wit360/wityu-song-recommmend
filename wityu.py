from   pymongo       import MongoClient
from   bson.objectid import ObjectId

"""
    handle all the talkings to mongodb wityudb
"""
class WityuDB:

    def __init__(self, host='localhost',port=27017):
        # connect to wityu db
        self.db = MongoClient(host, port).wityu_music_db

        # obtain collections
        self.users    = self.db.users
        self.songs    = self.db.songs
        self.userlogs = self.db.userlogs
        self.stations = self.db.stations
        self.docs     = {'users':self.users,'songs':self.songs,'userlogs':self.userlogs,'stations':self.stations}

    def get_stations(self):                  return [c['_id'] for c in self.stations.find({},{'_id':1})]
    def get_doc_by_id(self, doc, _id):       return self.docs[doc].find_one({'_id':_id})
    def get_station_by_id(self, station_id): return self.get_doc_by_id('stations',station_id)
    def get_song_by_id(self, station_id):    return self.get_doc_by_id('songs',station_id)
    def get_station_title_by_id(self, station_id): return self.get_station_by_id(station_id)['title']

    def get_user_set_by_actions(self, actions=[]):
        users = dict()
        for cursor in self.userlogs.find({'user':{'$exists':True},'action':{'$in':actions}},{'user':1,'action':1}):
            curr_action = cursor['action']
            curr_user   = cursor['user']
            if curr_action not in users: users[curr_action] = set()
            users[curr_action].add(curr_user)

        for cursor in self.userlogs.find({'session':{'$exists':True},'action':{'$in':actions}},{'session':1,'action':1}):
            curr_action = cursor['action']
            curr_user   = cursor['session']
            if curr_action not in users: users[curr_action] = set()
            users[curr_action].add(curr_user)
        return users

    def get_playStations_time(self):
        cursors = self.userlogs.find({'action':'playStation','loggedAt':{'$exists':True}},{'station':1}) #,'loggedAt':1})
        return [(c['station'],c['_id'].generation_time) for c in cursors]

    def get_playStations_by_user(self, user):
        output = []
        cursors = self.userlogs.find({'user':user,'action':'playStation'},{'station':1,'_id':1})
        prev=''
        for c in cursors:
            s = self.get_station_by_id(c['station'])
            if prev == s['_id']: continue
            prev = s['_id']
            output.append(c['station'])
        return output

    def update_station(self, sid, suggestion_doc):
        self.stations.update({'_id':sid}, {'$set':suggestion_doc});
