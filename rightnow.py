#!/usr/bin/python
from   wityu         import WityuDB
from   knn           import Knn
from   bson.objectid import ObjectId
from   collections   import Counter, defaultdict
from   datetime      import datetime

import heapq  # extract top k
import pickle # serialize log data
import math
import sys
import codecs
import locale

"""
"""
dayfeatures = ['mon','tue','wed','thu','fri','sat','sun']
#timefeatures = ['am1_00-06','am2_06-12','pm1_12-18','pm2_18-24']
timefeatures = ['am1_00-04','am2_04-08','am3_08-12','pm1_12-16','pm2_16-20','pm3_20-24']
#timefeatures = ['am1_00-03','am2_03-06','am3_06-09','am4_09-12','pm1_12-15','pm2_15-18','pm3_18-21','pm4_21-24']
combinedfeatures = list()

def main():

    wityu        = WityuDB('localhost', 27017)
    model_knn    = Knn()
    playStations = None
    stations     = defaultdict(lambda:Counter())
    c_stations   = defaultdict(lambda:Counter())

    # save time for preprocessing userlogs
    try:
        #raise IOError() # force rereading database
        with open('play_stations','r')  as f: playStations = pickle.load(f)
    except IOError as e:
        print "Preprocessing playStation data..."
        playStations = wityu.get_playStations_time()
        with open('play_stations','wb') as f: pickle.dump(playStations,f)

    for t in playStations: 
        sid,dt = t
        """
        print wityu.get_station_title_by_id(playStations[k*i][0])
        extract_time_feature(playStations[k*i][1])
        print
        """
        features = extract_time_feature(dt)
        for f in features: 
            stations[sid][f] += 1 
            c_stations[sid][f] += 1 

    for f1 in dayfeatures:
        for f2 in timefeatures:
            combinedfeatures.append(f1+'_'+f2)

    popset = set()
    for i,sid in enumerate(stations):
        for f in stations[sid]: stations[sid][f] = math.log(1+stations[sid][f])
        norm2d = math.sqrt(sum([stations[sid][f]*stations[sid][f] for f in dayfeatures]))
        norm2t = math.sqrt(sum([stations[sid][f]*stations[sid][f] for f in timefeatures]))
        norm2c = math.sqrt(sum([stations[sid][f]*stations[sid][f] for f in timefeatures]))
        if norm2d < 1.2: popset.add(sid)
        for f in dayfeatures:  stations[sid][f] /= norm2d
        for f in timefeatures: stations[sid][f] /= norm2t
        for f in combinedfeatures: stations[sid][f] /= norm2c
        print i,wityu.get_station_title_by_id(sid), "     ",norm2d,norm2t,norm2c
        """
        for f in sorted(stations[sid],key=stations[sid].get,reverse=True): 
            print "   %-20s %5.3f" % (f,stations[sid][f])
        for f in sorted(dayfeatures,key=stations[sid].get,reverse=True):  print "   %-20s %5.3f" % (f,stations[sid][f])
        print
        for f in sorted(timefeatures,key=stations[sid].get,reverse=True): print "   %-20s %5.3f" % (f,stations[sid][f])
        print
        """
    
    for sid in popset: stations.pop(sid)

    print "top for day features"
    for f in dayfeatures:
        L = heapq.nlargest(5,[(stations[sid][f],sid) for sid in stations])
        for i,t in enumerate(L):
            score,max_sid = t
            print "  %-10s %d %5.3f (%02d)  %s" % (f,i+1,score,c_stations[max_sid][f],wityu.get_station_title_by_id(max_sid))
        print

    print "top for time features"
    for f in timefeatures:
        L = heapq.nlargest(5,[(stations[sid][f],sid) for sid in stations])
        for i,t in enumerate(L):
            score,max_sid = t
            print "  %-10s %d %5.3f (%02d)  %s" % (f,i+1,score,c_stations[max_sid][f],wityu.get_station_title_by_id(max_sid))
        print

    print "top for combined features"
    for f in combinedfeatures:
        L = heapq.nlargest(5,[(stations[sid][f],sid) for sid in stations])
        for i,t in enumerate(L):
            score,max_sid = t
            print "  %-10s %d %5.3f (%02d)  %s" % (f,i+1,score,c_stations[max_sid][f],wityu.get_station_title_by_id(max_sid))
        print
"""
    'weekday'
    'timeslot'
"""
def extract_time_feature(dt):
    wd = dt.date().weekday()
    hr = dt.hour+7
    ts = timefeatures[0]
    if hr>24: 
        hr = hr%24
        wd = (wd+1)%7
    elif  4<=hr< 8 :ts=timefeatures[1]
    elif  8<=hr<12 :ts=timefeatures[2]
    elif 12<=hr<16: ts=timefeatures[3]
    elif 16<=hr<20: ts=timefeatures[4]
    elif 20<=hr: ts=timefeatures[5]
    return {dayfeatures[wd]:1,ts:1,str(dayfeatures[wd]+'_'+ts):1}

if __name__ == '__main__':
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
    main()
