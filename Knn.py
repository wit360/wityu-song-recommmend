from   collections   import Counter, defaultdict
import math
import heapq

"""
    create vector for each station 1..N: [feature_1:1 feature_2:1 feature_n:1]
    compare all station's distance to station i
"""
class Knn:
    SIM_COSINE  = 0
    SIM_JACCARD = 1
    def __init__(self):
        # key = station, value = [feature:value vector e.g. userid/timeofday]
        self.stations   = defaultdict(lambda:Counter()) 
        self.dotcache   = {}
        self.setcache   = {}
        self.sim_fn_map = {0:self.cosine_similarity,1:self.jaccard_coefficient}

    def add_user_station_dict(self, user_stations):
        for user in user_stations:
            for station in user_stations[user]:
                self.stations[station][user] += 1

    def add_time_station_dict(self, time_station):
        pass
    
    def recommend_next_stations(self,sid_prev,k=5,sim_fn=0):
        candidates = []
        for sid_next in self.stations:
            if sid_prev == sid_next: continue
            fn = self.sim_fn_map[sim_fn]
            candidates.append( (fn(sid_prev,sid_next),sid_next) )
        return heapq.nlargest(k, candidates)

    def jaccard_coefficient(self,sid1,sid2):
        s1,s2 = self.get_set(sid1), self.get_set(sid2)
        if len(s1)==0 or len(s2)==0: return 0.0
        return float(len(s1&s2))/len(s1|s2)

    def get_set(self,sid):
        if sid in self.setcache: return self.setcache[sid]
        self.setcache[sid] = set( self.stations[sid].keys() )
        return self.setcache[sid]
        
    def cosine_similarity(self,sid1,sid2):
        len1 = math.sqrt( self.dot_product(sid1,sid1) )
        len2 = math.sqrt( self.dot_product(sid2,sid2) )
        if len1 == 0 or len2 == 0: return 0.0
        v1v2 = self.dot_product(sid1,sid2)
        return float(v1v2)/(len1*len2)
         
    def dot_product(self,sid1,sid2): 
        if   (sid1,sid2) in self.dotcache: return self.dotcache[(sid1,sid2)]
        elif (sid2,sid1) in self.dotcache: return self.dotcache[(sid2,sid1)]
        v1,v2 = self.stations[sid1],self.stations[sid2]
        val   = sum([v1[sid]*v2[sid] for sid in v1])
        self.dotcache[(sid1,sid2)] = val
        return val
